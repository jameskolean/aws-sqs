package com.codegreenllc.aws.sqs;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class SimpleMessage {
	String name;
	String objetId;
	String value;
}