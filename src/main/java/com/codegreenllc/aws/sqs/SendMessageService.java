package com.codegreenllc.aws.sqs;

import java.io.IOException;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class SendMessageService {
	@Autowired
	AmazonSQSClient amazonSQSClient;
	@Autowired
	protected ObjectMapper objectMapper;

	@Value("${sqs.url}")
	private String sqsUrl;

	public void send(final SimpleMessage simpleMessage) throws IOException {
		simpleMessage.setObjetId(UUID.randomUUID().toString());
		amazonSQSClient.sendMessage(new SendMessageRequest(sqsUrl, objectMapper.writeValueAsString(simpleMessage)));
	}
}