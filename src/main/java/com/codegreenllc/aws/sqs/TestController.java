package com.codegreenllc.aws.sqs;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class TestController {
	@Autowired
	private SendMessageService sendMessageService;

	@GetMapping()
	public String getAll(@RequestParam final String name, @RequestParam final String value) throws IOException {
		final SimpleMessage simpleMessage = SimpleMessage.builder().name(name).value(value).build();
		sendMessageService.send(simpleMessage);
		return "Message sent.";
	}

}
