package com.codegreenllc.aws.sqs;

import javax.jms.Session;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.support.destination.DynamicDestinationResolver;

import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQSClient;

@Configuration
@EnableJms
public class SqsConfiguration {
	@Value("${aws.accessKey}")
	private String awsAccessKey;

	@Value("${aws.region}")
	private String awsRegion;

	@Value("${aws.secretKey}")
	private String awsSecretKey;

	@Bean
	public AmazonSQSClient amazonSQSClient() {
		return new AmazonSQSClient(getAwsCredentials());
	}

	private AWSCredentials getAwsCredentials() {
		return new BasicAWSCredentials(awsAccessKey, awsSecretKey);
	}

	private AWSCredentialsProvider getAwsCredentialsProvider() {
		return new AWSCredentialsProvider() {
			@Override
			public AWSCredentials getCredentials() {
				return getAwsCredentials();
			}

			@Override
			public void refresh() {
			}
		};
	}

	@Bean
	public DefaultJmsListenerContainerFactory jmsListenerContainerFactory() {
		final SQSConnectionFactory connectionFactory = SQSConnectionFactory.builder()
				.withRegion(Region.getRegion(Regions.fromName(awsRegion)))
				.withAWSCredentialsProvider(getAwsCredentialsProvider()).build();

		final DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
		factory.setConnectionFactory(connectionFactory);
		factory.setDestinationResolver(new DynamicDestinationResolver());
		factory.setConcurrency("3-10");
		factory.setSessionAcknowledgeMode(Session.CLIENT_ACKNOWLEDGE);
		return factory;
	}

}
